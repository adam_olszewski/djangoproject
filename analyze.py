import hashlib
import json
import sys

from SPARQLWrapper import SPARQLWrapper, JSON
import xml.etree.ElementTree as ET
from elasticsearch6 import Elasticsearch

XML_FILE_PATH = '/home/adam/gitlab_projects/zti-sparql-dbpedia/QALD-master/2/data/dbpedia-train.xml'

root = ET.parse(XML_FILE_PATH).getroot()


class DataRow:
    def __init__(self, answer_type, aggregation, onlydbo, query_string, keywords, query_sparql):
        self.query_sparql = query_sparql
        self.keywords = keywords
        self.query_string = query_string
        self.onlydbo = onlydbo
        self.aggregation = aggregation
        self.answer_type = answer_type


def fetch_and_normalize_row(row):
    answer_type = row.get('answertype').strip()
    aggregation = row.get('aggregation').strip()
    onlydbo = row.get('onlydbo').strip()

    query_string = row.findall('string')[0].text.strip()
    keywords = row.findall('keywords')[0].text.strip().split(',')
    keywords = [x.strip(' ') for x in keywords]
    query_sparql = row.findall('query')[0].text

    return DataRow(answer_type, aggregation, onlydbo, query_string, keywords, query_sparql)


def read_dataset():
    rows = []
    for type_tag in root.findall('question'):
        rows.append(fetch_and_normalize_row(type_tag))
    return rows


def sparql_query(data_row):
    try:
        sparql = SPARQLWrapper("http://dbpedia.org/sparql")
        sparql.setQuery('{query}'.format(query=data_row['hits']['hits'][0]['_source']['query_sparql']))
        sparql.setReturnFormat(JSON)
        return sparql.query().convert()
    except IndexError:
        return "Not sparql given"


data_rows = read_dataset()
es = Elasticsearch()

for data_row in data_rows:
    doc_id = hashlib.md5(data_row.query_string.encode('utf-8')).hexdigest()
    res = es.index(index="zti-labs", id=doc_id, body=json.dumps(data_row.__dict__), doc_type='query_row')


def search(body):
    return es.search(index="zti-labs", body=body)


name_term = 'test'
query_body = {
    "query": {
        "query_string": {
            "query": f"(query_string:{name_term})^10 AND keywords:({name_term})^9"
        }
    },
    "from": 0,
    "size": 1
}
a = search(query_body)
print(a)
print(sparql_query(a))
