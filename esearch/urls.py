from django.urls import path
from . import views
from django.contrib import admin
app_name = 'esearch'
urlpatterns = [
    path('', views.search_index, name='search_view'),
    #path('admin/', admin.site.urls),
]