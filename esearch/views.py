from django.shortcuts import render
from .call import esearch
from analyze import search, sparql_query


def search_index(request):
    results = []
    name_term = ""
    if request.GET.get('name'):
        name_term = request.GET['name']
    search_term = name_term
    if name_term == '':
        name_term = 'test'
    query_body = {
        "query": {
            "query_string": {
                "query": f"(query_string:{name_term})^10 AND keywords:({name_term})^9"
            }
        },
        "from": 0,
        "size": 1
    }
    results_elastic = search(query_body)
    print(results_elastic)
    results_dbpedia = sparql_query(results_elastic)
    print(results_dbpedia)
    context = {'results_elastic': results_elastic, 'results_dbpedia': results_dbpedia, 'search_term': search_term }
    return render(request, 'esearch/index.html', context)



