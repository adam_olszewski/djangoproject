from django.shortcuts import render
from .call import esearch
from analyze import search

def search_index(request):
    results = []
    name_term = ""
    if request.GET.get('name'):
        name_term = request.GET['name']
    search_term = name_term
    query_body = {
        "query": {
            "query_string": {
                "query": "(query_string:" + search_term + ")^10 AND keywords:(*)^9"
            }
        },
        "from": 0,
        "size": 1
    }

    results = search(body=query_body)
    print(results)
    context = {'results': results, 'count': len(results), 'search_term':  search_term}
    return render(request,  'esearch/index.html',  context)